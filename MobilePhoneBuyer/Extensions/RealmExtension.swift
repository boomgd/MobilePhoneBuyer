//
//  RealmExtension.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import RealmSwift

extension Object {
    
    func save() throws {
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(self)
            }
        } catch {
            throw ProductListError.CannotSave
        }
        
        
        
    }
    
    class func get() throws -> [Object]? {
        
        do {
            let realm = try Realm()
            let results = realm.objects(self)
            
            return Array(results)
        } catch  {
            throw ProductListError.CannotFetch
        }
        
    }
    
    
    func delete() throws {
        
        do {
            
            let realm = try Realm()

            try realm.write {
                realm.delete(self)
            }
        } catch  {
            throw ProductListError.CannotDelete
        }
    }
}
