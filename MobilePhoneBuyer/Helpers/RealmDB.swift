//
//  RealmDB.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import RealmSwift
class RealmDB {
    
    class func save<T: Object>(objects: [T]) {
        do {
            let realm = try Realm()
            
            try realm.write {
                
                for item in objects {
                    realm.add(item, update: true)
                }
                
            }
            
        } catch {
            
        }
    }
    
    
}
