//
//  ProductDetailModels.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

struct ProductDetail {
    enum FetchImage {
        
        struct Request {
            var id: Int
        }
        
        struct Response {
            var productImages: [URL]
            var product: Product
        }
        
        struct ViewModel {
            struct DisplayedProductDetail {
                var thumbnailUrls: [URL]?
                var name: String
                var description: String
                var price: String
                var rating: String
            }
            
            let displayedProduct: DisplayedProductDetail
        }
    }
}
