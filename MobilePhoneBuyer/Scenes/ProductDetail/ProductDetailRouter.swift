//
//  ProductDetailRouter.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

protocol ProductDetailDataPassing {
    var dataStore: ProductDetailDataStore? { get }
}

class ProductDetailRouter: ProductDetailDataPassing {
    
    weak var viewController: ProductDetailViewController?
    var dataStore: ProductDetailDataStore?
    
    
}
