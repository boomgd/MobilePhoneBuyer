//
//  ProductDetailWorker.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

enum ProductDetailResult<T> {
    case Success(result: T)
    case Failure(error: ProductDetailError)
}

enum ProductDetailError: Equatable, Error {
    case ServerError
    case BadNetwork
}

class ProductDetailWorker {
    
    private var service: ProductDetailServiceInterface!
    
    deinit {
        print("Product detail Worker👨🏼‍💻 free")
    }
    
    init(service: ProductDetailServiceInterface) {
        self.service = service
    }
    
    func getProductImages(id: String, completionHandler: @escaping (ProductDetailResult<[ProductImage]>) -> Void) {
        
        service.getProductImages(id: id) { (result: ProductDetailResult<[ProductImage]>) in
            completionHandler(result)
        }
        
    }
    
}
