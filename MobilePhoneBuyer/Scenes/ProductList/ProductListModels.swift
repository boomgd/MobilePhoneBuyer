//
//  ProductListModels.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

enum ProductList {
    // MARK: Use cases
    enum FetchProducts {
        
        struct Request {
            
        }
        
        struct Response {
            var products: [Product]
        }
        
        struct ViewModel {
            struct DisplayedProduct {
                var thumbnailUrl: URL?
                var name: String
                var description: String
                var price: String
                var rating: String
                var isFavorite: Bool
            }
            
            let displayedProducts: [DisplayedProduct]
        }
    }
    
    enum SaveProduct {
        struct Request {
            var index: Int
        }
        
        struct Response {
           
        }
        
        struct ViewModel {
        
        }
    }
}
