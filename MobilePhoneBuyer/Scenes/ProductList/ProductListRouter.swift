//
//  ProductListRouter.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/9/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import UIKit
protocol ProductListRoutingLogic {
    ///Call when need to push to show product detail page
    func routeToShowProductDetail(segue: UIStoryboardSegue?)
}

protocol ProductListDataPassing {
    var dataStore: ProductListDataStore? { get }
}

class ProductListRouter: ProductListRoutingLogic, ProductListDataPassing {

    weak var viewController: ProductListViewController?
    var dataStore: ProductListDataStore?
    
    func routeToShowProductDetail(segue: UIStoryboardSegue?) {
    
        if let destinationVC = segue?.destination as? ProductDetailViewController, let dataStore = self.dataStore {
            var destinationDatastore = destinationVC.router?.dataStore
            if let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row {
                
                viewController?.tableView.deselectRow(at: IndexPath(row: selectedRow, section: 0), animated: true)

                var products: [Product] = []
                
                switch dataStore.currentTabbarType {
                case .all:
                    //If current tabbarType is all use all product list
                    products = dataStore.products
                case .favorite:
                    //If current tabbarType is all use favorite list
                    products = dataStore.favoriteProducts

                }
                
                destinationDatastore?.product = products[selectedRow]
            }
        }

    }
    
}
