//
//  ProductDetailRequest.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import Alamofire
class ProductDetailRequest: APIRequest {
    
    var productId: String = ""
    
    override var url :  String {
        return baseUrl + "mobiles/" + productId + "/images/"
    }
    
    override var method: Alamofire.HTTPMethod {
        return .get
    }
    
    override var paramsType: APIParameterType {
        return .json
    }
    
    override var params: [String : Any]? {
        return nil
    }
    
    override var responseKeyPath: String {
        return ""
    }
    
}
