//
//  ProductDetailService.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation

typealias ProductDetailGetImageCompletionHandler = (ProductDetailResult<[ProductImage]>) -> Void

protocol ProductDetailServiceInterface {
    func getProductImages(id: String, completionHandler: @escaping ProductDetailGetImageCompletionHandler)
}

class ProductDetailService: ProductDetailServiceInterface {
    
    func getProductImages(id: String, completionHandler: @escaping (ProductDetailResult<[ProductImage]>) -> Void) {
        
        let request = ProductDetailRequest()
        request.productId = id
        API.requestForItems(request: request) { (items: [ProductImage]?, error) in
            if let items = items {
                completionHandler(ProductDetailResult.Success(result: items))
            } else {
                completionHandler(ProductDetailResult.Failure(error: ProductDetailError.ServerError))
            }
        }
        
    }
    
}
