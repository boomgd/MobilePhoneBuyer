//
//  ProductListRequest.swift
//  MobilePhoneBuyer
//
//  Created by Watcharavit Lapinee on 2/10/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

import Foundation
import Alamofire
class ProductListRequest: APIRequest {

    override var url :  String {
        return baseUrl + "mobiles/"
    }
    
    override var method: Alamofire.HTTPMethod {
        return .get
    }
    
    override var paramsType: APIParameterType {
        return .json
    }
    
    override var params: [String : Any]? {
        return nil
    }
    
    override var responseKeyPath: String {
        return ""
    }
    
}
