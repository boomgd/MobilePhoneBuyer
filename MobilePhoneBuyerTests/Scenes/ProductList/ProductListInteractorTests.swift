//
//  ProductListInteractorTests.swift
//  MobilePhoneBuyerTests
//
//  Created by Watcharavit Lapinee on 2/11/2561 BE.
//  Copyright © 2561 Watcharavit Lapinee. All rights reserved.
//

@testable import MobilePhoneBuyer
import XCTest

//MARK: - Mockup presenter protocol
class MockupProductListPresentation: ProductListPresentationLogic {
    
    var presentFetchedProductsCalled = false
    var presentErrorCalled = false
    var presentToastCalled = false
    var presentFailToSaveAlertCalled = false
    
    func presentFetchedProducts(response: ProductList.FetchProducts.Response) {
        presentFetchedProductsCalled = true
    }
    
    func presentError(error: ProductListError) {
        presentErrorCalled = true
    }
    
    func presentToast(message: String) {
        presentToastCalled = true
    }
    
    func presentFailToSaveAlert(error: ProductListError) {
        presentFailToSaveAlertCalled = true
    }
    
    
}

//MARK: - Mockup worker class
class MockupProductListWorker: ProductListWorker {
    
    var fetchFavoriteCalled = false
    var fetchProductsCalled = false
    var setToFavoriteCalled = false
    var removeFromFavoriteCalled = false

    override func fetchFavorite(completionHandler: @escaping ProductListFetchCompletionHandler) {
        fetchFavoriteCalled = true
        
        var products: [Product] = []
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = false
        
        products.append(product)
        
        completionHandler(ProductListResult.Success(result: products))
    }
    
    override func fetchProducts(completionHandler: @escaping ProductListFetchCompletionHandler) {
        fetchProductsCalled = true
        
        var products: [Product] = []
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = false
        
        products.append(product)
        
        completionHandler(ProductListResult.Success(result: products))
    }
    
    override func setToFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        setToFavoriteCalled = true
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        completionHandler(ProductListResult.Success(result: product))
    }
    
    override func removeFromFavorite(product: Product, completionHandler: @escaping ProductListDBCompletionHandler) {
        removeFromFavoriteCalled = true
        
        let product = Product()
        product.brand = "Nokia"
        product.detail = "Test detail"
        product.name = "Nokia 6"
        product.id = 2
        product.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        product.rating = 4.6
        product.price = 199.99
        product.isFavorite = true
        
        completionHandler(ProductListResult.Success(result: product))
    }
    
}

class ProductListInteractorTests: XCTestCase {
    
    var interactor: ProductListInteractor!
    static var products: [Product]!
    
    override func setUp() {
        super.setUp()
        
        setupInteractor()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //MARK: - Setup
    func setupInteractor() {
        
        interactor = ProductListInteractor()
        
        let productNotFavorite = Product()
        productNotFavorite.brand = "Nokia"
        productNotFavorite.detail = "Test detail"
        productNotFavorite.name = "Nokia 6"
        productNotFavorite.id = 2
        productNotFavorite.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        productNotFavorite.rating = 4.6
        productNotFavorite.price = 199.99
        productNotFavorite.isFavorite = false
        
        let productFavorite = Product()
        productFavorite.brand = "Sony"
        productFavorite.detail = "Test detail"
        productFavorite.name = "Sony 6"
        productFavorite.id = 3
        productFavorite.thumbImageUrl = "https://cdn.mos.cms.futurecdn.net/8LWUERoxMAWavvVAAbxuac-650-80.jpg"
        productFavorite.rating = 4.6
        productFavorite.price = 179.99
        productFavorite.isFavorite = true
        
        
        ProductListInteractorTests.products = [productNotFavorite, productFavorite]
        
    }
    
    //MARK: - Tests
    
    func testFetchProductsShouldCallProductListWorkerToFetchProductsAndPresentToast() {
        
        //Given
        let presentationLogic = MockupProductListPresentation()
        interactor.presenter = presentationLogic
        let service = MockupProductListSerivce()
        
        let worker = MockupProductListWorker(service: service)
        interactor.worker = worker
        
        //When
        let request = ProductList.FetchProducts.Request()
        interactor.fetchProducts(request: request, needUpdate: true)
        
        //Then
        XCTAssert(worker.fetchProductsCalled, "FetchProducts() should ask Worker to fetch products")
        XCTAssert(presentationLogic.presentFetchedProductsCalled, "FetchProducts() should ask presenter to prepare viewModel")
    }
    
    func  testSaveToFavoriteShouldCallProductListWorkerToSaveFavoriteSuccessAndPresentToast() {
        
        //Given
        let presentationLogic = MockupProductListPresentation()
        interactor.presenter = presentationLogic
        interactor.products = ProductListInteractorTests.products
        let service = MockupProductListSerivce()
        
        let worker = MockupProductListWorker(service: service)
        interactor.worker = worker
        
        //When
        let request = ProductList.SaveProduct.Request(index: 0)
        interactor.setToFavorite(request: request, tabbarType: .all)
        
        //Then
        XCTAssert(worker.setToFavoriteCalled, "setToFavorite() should ask Worker to save that product to favorite list")
        XCTAssert(presentationLogic.presentToastCalled, "setToFavorite() should ask presenter to show toast")
    }
    
    func  testRemoveFromFavoriteShouldCallProductListWorkerToRemoveFavoriteSuccessAndPresentToast() {
        
        //Given
        let presentationLogic = MockupProductListPresentation()
        interactor.presenter = presentationLogic
        interactor.products = ProductListInteractorTests.products
        let service = MockupProductListSerivce()
        
        let worker = MockupProductListWorker(service: service)
        interactor.worker = worker
        
        //When
        let request = ProductList.SaveProduct.Request(index: 1)
        interactor.setToFavorite(request: request, tabbarType: .all)
        
        //Then
        XCTAssert(worker.removeFromFavoriteCalled, "removeFromFavorite() should ask Worker to remove that product from favorite list")
        XCTAssert(presentationLogic.presentToastCalled, "removeFromFavorite() should ask presenter to show toast")
    }
    
}
